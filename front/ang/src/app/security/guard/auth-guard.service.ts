import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {LoginService} from '../login.service';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

    constructor(private router: Router, private loginService: LoginService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.checkToken(state);
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.checkToken(state);
    }

    private checkToken(state: RouterStateSnapshot): boolean {
        const isUserLogged = this.loginService.isLoggedIn();
        if (!isUserLogged) {
            this.loginService.logout();
            this.router.navigate(['/login'], {
                queryParams: {
                    previousUrl: state.url
                }
            });
        }
        return isUserLogged;
    }
}
