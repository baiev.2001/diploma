import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {LoginService} from '../login.service';
import {Injectable} from '@angular/core';
import {Location} from '@angular/common';

@Injectable()
export class LoggedInGuardService implements CanActivate, CanActivateChild {

  constructor(private loginService: LoginService,
              private location: Location) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.isLoggedIn();
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.isLoggedIn();
  }

  private isLoggedIn(): boolean {
    if (this.loginService.isLoggedIn()) {
      this.location.back();
      return false;
    }
    return true;
  }
}
