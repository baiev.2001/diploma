import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {ApiTokenInterceptor} from "./token-interceptor";

export const INTERCEPTOR_PROVIDERS = [
  {provide: HTTP_INTERCEPTORS, useClass: ApiTokenInterceptor, multi: true}
]
