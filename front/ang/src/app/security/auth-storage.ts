import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

export const AUTH_TOKEN_KEY = 'auth-token';

@Injectable()
export class AuthStorage {

    private storage = localStorage;
    private jwtHelperService = new JwtHelperService();

    public store(token: string): void {
        this.storage.setItem(AUTH_TOKEN_KEY, token);
    }

    public get(): string {
        return this.storage.getItem(AUTH_TOKEN_KEY) || '';
    }

    public remove(): void {
        this.storage.removeItem(AUTH_TOKEN_KEY);
    }

    public isTokenExpired(): boolean {
      // console.log("TOKEN::::");
      // console.log(this.get());
        return this.jwtHelperService.isTokenExpired(this.get());
    }

    public getUserId(): any {
        const token = this.get();
        return token && this.jwtHelperService.decodeToken(token).sub;
    }
}
