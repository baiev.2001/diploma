export class LoginData {
  login: string;
  password: string;

  constructor() {
    this.login = '';
    this.password = ''
  }
}

export class AuthData {
  authToken: string;
  expire: string;

  constructor() {
    this.authToken = '';
    this.expire = '';
  }
}
