import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthData, LoginData} from './login-data';
import {Observable} from 'rxjs';
import {AuthStorage} from './auth-storage';
import {map} from 'rxjs/operators';

@Injectable()
export class LoginService {
  constructor(private http: HttpClient,
              private storage: AuthStorage) {
  }

  public login(data: LoginData): Observable<any> {
    return this.http.post<AuthData>('/api/auth/login', data)
      .pipe(map(response => {
        // console.warn(response)
        if (response && response.authToken) {
          this.storage.store(response.authToken);
        }
        return response.authToken;
      }));
  }

  public logout(): void {
    this.storage.remove();
  }

  public isLoggedIn(): boolean {
    const token = this.storage.get();
    // console.log(this.storage.isTokenExpired());
    return !!token
      && token !== ''
      && !this.storage.isTokenExpired();
  }

  public getToken(): string {
    return this.storage.get();
  }

  public clearToken(): void {
    this.storage.remove();
  }
}
