import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {LoginService} from './login.service';

@Injectable()
export class ApiTokenInterceptor implements HttpInterceptor {

    constructor(private router: Router,
                private loginService: LoginService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const isUserLogged = this.loginService.isLoggedIn();
        if (!req.url.includes('/auth')) {
            if (isUserLogged) {
                req = req.clone({
                    setHeaders: {
                        Authorization: `${this.loginService.getToken()}`
                    }
                });
                console.warn(req);
            } else {
                this.loginService.logout();
                this.router.navigate(['/login']);
            }
        }

        return next.handle(req).pipe(map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {

            }
            return event;

        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    this.router.navigate(['/login']);
                }

                if (err.status === 412) {
                    this.router.navigate(['/login']);
                }
            }
        }));

    }
}
