import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DeviceListComponent} from "./device-list/device-list.component";
import {DeviceDetailsComponent} from "./device-details/device-details.component";
import {LoginComponent} from "./login/login.component";
import {AuthGuardService} from "./security/guard/auth-guard.service";
import {LoggedInGuardService} from "./security/guard/logged-in-guard.service";

const routes: Routes = [
  {
    path: 'device',
    component: DeviceListComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'device/:id',
    component: DeviceDetailsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoggedInGuardService]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'device'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
