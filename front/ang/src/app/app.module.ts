import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DeviceListComponent} from './device-list/device-list.component';
import {NavMenuComponent} from './nav-menu/nav-menu.component';
import {DeviceDetailsComponent} from './device-details/device-details.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DeviceService} from "./service/device-service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {LoginComponent} from './login/login.component';
import {LoggedInGuardService} from "./security/guard/logged-in-guard.service";
import {AuthGuardService} from "./security/guard/auth-guard.service";
import {LoginService} from "./security/login.service";
import {AuthStorage} from "./security/auth-storage";
import {INTERCEPTOR_PROVIDERS} from "./security";

@NgModule({
  declarations: [
    AppComponent,
    DeviceListComponent,
    NavMenuComponent,
    DeviceDetailsComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    DeviceService,
    HttpClient,
    LoggedInGuardService,
    AuthGuardService,
    LoginService,
    INTERCEPTOR_PROVIDERS,
    AuthStorage
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
