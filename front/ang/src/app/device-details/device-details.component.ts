import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DeviceDetails} from "../service/device";
import {DeviceService} from "../service/device-service";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.scss']
})
export class DeviceDetailsComponent implements OnInit {
  deviceDetailsForm: FormGroup;
  device: DeviceDetails = new DeviceDetails();
  id: number;
  editable: boolean = false;
  updatedName: string = '';

  constructor(private formBuilder: FormBuilder,
              private deviceService: DeviceService,
              private router: Router,
              private activateRoute: ActivatedRoute) {
    this.id = activateRoute.snapshot.params['id'];
    this.deviceDetailsForm = formBuilder.group({
      alias: [''],
      type: [''],
      currentValue: ['']
    })
  }


  ngOnInit(): void {
    this.deviceService.getDeviceDetails(this.id)
      .subscribe(resp => {
        this.device = resp;
      }, error => {
        console.log(error);
      });
  }

  updateName() {
    this.device.chipId = this.updatedName;
    this.editable = false;
  }

  cancelUpdate() {
    this.updatedName = '';
    this.editable = false;
  }

  back() {
    this.router.navigate(['device']);
  }

  edit() {
    this.updatedName = this.device.chipId;
    this.editable = true;
  }
}
