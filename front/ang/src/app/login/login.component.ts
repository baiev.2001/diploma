import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginData} from "../security/login-data";
import {LoginService} from "../security/login.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private static DEFAULT_PATH = '/device';

  private redirectUrl: string = '';
  errorMessage: string = '';
  loginForm: FormGroup;

  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private route: ActivatedRoute,
              private router: Router) {
    this.loginForm = fb.group({
        login: ['', Validators.required],
        password: ['', Validators.required]
      }
    );
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.redirectUrl = params['previousUrl'] || LoginComponent.DEFAULT_PATH;
      console.warn(this.redirectUrl);
    });
  }

  onSubmit(): void {
    const controls = this.loginForm.controls;
    const data = new LoginData();
    data.login = controls['login'].value;
    data.password = controls['password'].value;
    this.loginService.login(data)
      .subscribe(resp => {
        console.log(resp);
        console.log(this.redirectUrl);
        this.router.navigate([this.redirectUrl]);
      }, error => {
        this.errorMessage = error.error.message;
      });
  }

}
