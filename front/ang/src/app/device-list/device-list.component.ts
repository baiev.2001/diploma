import {Component, OnInit} from '@angular/core';
import {Device} from "../service/device";
import {Router} from "@angular/router";
import {DeviceService} from "../service/device-service";

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss']
})
export class DeviceListComponent implements OnInit {

  devices: Device[] = [];

  constructor(private router: Router,
              private deviceService: DeviceService) {
  }

  ngOnInit(): void {
    this.deviceService.getDevices()
      .subscribe((resp) => {
          this.devices = resp;
        },
        err => {
          console.log(err)
        }
      );
  }

  showDetails(device: Device) {
    this.router.navigate(['device', device.id]);
  }
}
