import {Device, DeviceDetails} from "./device";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable()
export class DeviceService {

  constructor(private http: HttpClient) {
  }

  public getDevices(): Observable<Device[]> {
    return this.http.get<Device[]>('/api/device');
  }

  public getDeviceDetails(id: number): Observable<DeviceDetails> {
    return this.http.get<DeviceDetails>(`/api/device/${id}`);
  }
}
