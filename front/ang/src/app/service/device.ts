export class Device {
  id?: number;
  name: string;
  type?: string;
  value?: number;
  unit?: string;


  constructor() {
    this.id = 0;
    this.name = 'undefined';
  }
}

export class Data {
  value?: number;
  unit?: string;
  deviceChipId?: string;
  time?: string;

  toString() {
    return `${this.value}`
  }
}

export class DeviceDetails {
  chipId: string;
  type: string;
  deviceData: Data[] = [];


  constructor() {
    this.chipId = '';
    this.type = '';
  }
}
