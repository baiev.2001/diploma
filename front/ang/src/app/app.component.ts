import {Component} from '@angular/core';
import {LoginService} from "./security/login.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'diplom';


  constructor(private loginService: LoginService) {

  }

  logout() {
    console.log("LOGOUT");
    this.loginService.logout();
    window.location.reload();
  }

  isLogged(): boolean {
    return this.loginService.isLoggedIn();
  }
}
