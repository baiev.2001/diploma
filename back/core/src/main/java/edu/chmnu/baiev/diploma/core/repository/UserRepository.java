package edu.chmnu.baiev.diploma.core.repository;

import edu.chmnu.baiev.diploma.core.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByLogin(@Param("login") String login);
}
