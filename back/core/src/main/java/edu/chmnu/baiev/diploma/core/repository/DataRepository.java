package edu.chmnu.baiev.diploma.core.repository;

import edu.chmnu.baiev.diploma.core.entity.Data;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DataRepository extends JpaRepository<Data, Long> {

    @Query("select d from Data d where d.device.id = :id")
    List<Data> findByDevice(@Param("id") Long deviceId);
}
