package edu.chmnu.baiev.diploma.core.repository;

import edu.chmnu.baiev.diploma.core.entity.DeviceType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceTypeRepository extends JpaRepository<DeviceType, Long> {
}
