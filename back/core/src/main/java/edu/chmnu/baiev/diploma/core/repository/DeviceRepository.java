package edu.chmnu.baiev.diploma.core.repository;

import edu.chmnu.baiev.diploma.core.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceRepository extends JpaRepository<Device, Long> {

    Device findByChipId(String chipId);
}
