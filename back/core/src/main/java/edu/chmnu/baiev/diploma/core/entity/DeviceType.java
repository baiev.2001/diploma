package edu.chmnu.baiev.diploma.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "device_type")
public class DeviceType {
    public static final DeviceType TEMPERATURE = new DeviceType(1, "temperature", "Cels");
    public static final DeviceType LIGHT = new DeviceType(2, "light", "Lumen");
    public static final DeviceType A_P = new DeviceType(3, "access_point", null);

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "unit")
    private String unit;


    public static DeviceType of(Integer id) {
        return values().stream()
                .filter(type -> type.id.equals(id))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Can't find type for id: " + id));
    }

    public static Collection<DeviceType> values() {
        return Arrays.asList(TEMPERATURE,
                LIGHT,
                A_P);
    }
}
