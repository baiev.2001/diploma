package edu.chmnu.baiev.diploma.user.data;

import edu.chmnu.baiev.diploma.user.data.service.DataDTO;
import edu.chmnu.baiev.diploma.user.data.service.DataService;
import edu.chmnu.baiev.diploma.user.data.service.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping
public class DataController {
    private final DeviceService deviceService;
    private final DataService dataService;

    @GetMapping("/data")
    public List<DataDTO> getAllData() {
        return dataService.getAllData();
    }
}
