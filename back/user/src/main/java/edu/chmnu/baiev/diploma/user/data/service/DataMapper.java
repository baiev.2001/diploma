package edu.chmnu.baiev.diploma.user.data.service;

import edu.chmnu.baiev.diploma.core.entity.Data;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;

@Component
public class DataMapper {

    private final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss");

    public DataDTO map(Data data) {
        DataDTO dto = new DataDTO();
        dto.setValue(data.getVal());
        dto.setUnit(data.getDevice().getType().getUnit());
        dto.setDeviceChipId(data.getDevice().getChipId());
        dto.setTime(DATE_TIME_FORMATTER.format(data.getTime()));
        return dto;
    }
}
