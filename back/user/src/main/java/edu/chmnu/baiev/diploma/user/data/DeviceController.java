package edu.chmnu.baiev.diploma.user.data;

import edu.chmnu.baiev.diploma.user.data.service.DeviceDetails;
import edu.chmnu.baiev.diploma.user.data.service.DeviceDto;
import edu.chmnu.baiev.diploma.user.data.service.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/device")
@RequiredArgsConstructor
public class DeviceController {
    private final DeviceService deviceService;

    @GetMapping()
    public List<DeviceDto> getDevices() {
        return deviceService.getAllDevices();
    }

    @GetMapping("/{id}")
    public DeviceDetails getDevices(@PathVariable("id") Long id) {
        System.out.println(id);
        return deviceService.getDeviceDetails(id);
    }
}
