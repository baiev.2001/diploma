package edu.chmnu.baiev.diploma.user.security.service;

import edu.chmnu.baiev.diploma.core.entity.User;
import edu.chmnu.baiev.diploma.core.repository.UserRepository;
import edu.chmnu.baiev.diploma.user.security.SecuredUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByLogin(username);
        if (user.isPresent()) {
            return new SecuredUserDetails(user.get());
        }
        throw new UsernameNotFoundException("Can't find user by name " + username);
    }
}
