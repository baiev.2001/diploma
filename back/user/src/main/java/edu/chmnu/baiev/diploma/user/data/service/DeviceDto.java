package edu.chmnu.baiev.diploma.user.data.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeviceDto {
    private Long id;

    private String name;

    private String chipId;

    private String type;

    private Double value;

    private String unit;
}
