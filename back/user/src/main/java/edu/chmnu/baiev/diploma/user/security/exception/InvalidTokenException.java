package edu.chmnu.baiev.diploma.user.security.exception;

public class InvalidTokenException extends RuntimeException {
    public InvalidTokenException(String token, Exception ex) {
        super("Invalid token: " + token, ex);
    }
}
