package edu.chmnu.baiev.diploma.user.security.exception;

public class SecurityDetailsNotFoundException extends RuntimeException {
    public SecurityDetailsNotFoundException(String s) {
        super(s);
    }
}
