package edu.chmnu.baiev.diploma.user.data.service;

import edu.chmnu.baiev.diploma.core.entity.Device;

import java.util.List;

public interface DeviceService {

    List<DeviceDto> getAllDevices();

    DeviceDetails getDeviceDetails(Long id);

    DeviceDetails getDeviceDetails(String chipId);
}
