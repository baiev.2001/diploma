package edu.chmnu.baiev.diploma.user.security;

import edu.chmnu.baiev.diploma.user.security.service.UserSecurityService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final UserSecurityService securityService;

    @PostMapping("/login")
    public AuthenticationDto login(@RequestBody @Valid LoginDto dto) {
        return securityService.login(dto.getLogin(), dto.getPassword());
    }
}
