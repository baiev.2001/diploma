package edu.chmnu.baiev.diploma.user.data.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataDTO {

    private Double value;

    private String unit;

    private String deviceChipId;

    private String time;
}
