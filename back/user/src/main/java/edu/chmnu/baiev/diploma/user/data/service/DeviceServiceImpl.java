package edu.chmnu.baiev.diploma.user.data.service;

import edu.chmnu.baiev.diploma.core.entity.Device;
import edu.chmnu.baiev.diploma.core.repository.DeviceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DeviceServiceImpl implements DeviceService {
    private final DeviceRepository deviceRepository;
    private final DeviceMapper deviceMapper;
    private final DeviceDetailsMapper deviceDetailsMapper;

    @Override
    public List<DeviceDto> getAllDevices() {
        List<Device> devices = deviceRepository.findAll();

        return devices.stream()
                .map(deviceMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public DeviceDetails getDeviceDetails(Long id) {
        Device device = deviceRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Can't find device by id: " + id));

        return deviceDetailsMapper.map(device);
    }

    @Override
    public DeviceDetails getDeviceDetails(String chipId) {
        return null;
    }
}
