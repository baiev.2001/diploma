package edu.chmnu.baiev.diploma.user.data.service;

import java.time.LocalDateTime;
import java.util.List;

public interface DataService {

    List<DataDTO> getAllData();

    List<DataDTO> getAllData(LocalDateTime from);

    List<DataDTO> getAllData(LocalDateTime from, LocalDateTime to);

    List<DataDTO> getData(String chipId);

    List<DataDTO> getData(String chipId, LocalDateTime from);

    List<DataDTO> getData(String chipId, LocalDateTime from, LocalDateTime to);

}
