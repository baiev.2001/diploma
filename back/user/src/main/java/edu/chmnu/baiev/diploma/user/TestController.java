package edu.chmnu.baiev.diploma.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class TestController {


    @GetMapping(path = "/test")
    public String helloWorld() {
        return "Hello world!";
    }
}
