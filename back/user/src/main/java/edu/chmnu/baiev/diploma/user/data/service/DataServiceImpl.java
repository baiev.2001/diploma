package edu.chmnu.baiev.diploma.user.data.service;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class DataServiceImpl implements DataService {
    @Override
    public List<DataDTO> getAllData() {
        return null;
    }

    @Override
    public List<DataDTO> getAllData(LocalDateTime from) {
        return null;
    }

    @Override
    public List<DataDTO> getAllData(LocalDateTime from, LocalDateTime to) {
        return null;
    }

    @Override
    public List<DataDTO> getData(String chipId) {
        return null;
    }

    @Override
    public List<DataDTO> getData(String chipId, LocalDateTime from) {
        return null;
    }

    @Override
    public List<DataDTO> getData(String chipId, LocalDateTime from, LocalDateTime to) {
        return null;
    }
}
