package edu.chmnu.baiev.diploma.user.security.service;

import edu.chmnu.baiev.diploma.core.entity.User;
import edu.chmnu.baiev.diploma.core.repository.UserRepository;
import edu.chmnu.baiev.diploma.user.security.AuthenticationDto;
import edu.chmnu.baiev.diploma.user.security.SecuredUserDetails;
import edu.chmnu.baiev.diploma.user.security.exception.SecurityDetailsNotFoundException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.util.Date;

@Service
public class UserSecurityServiceImpl implements UserSecurityService {
    private final UserRepository repository;
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtTokenUtil;
    private final PasswordEncoder passwordEncoder;
    private final SecurityContextRepository securityContextRepository;

    public UserSecurityServiceImpl(UserRepository repository,
                                   AuthenticationManager authenticationManager,
                                   JwtUtil jwtTokenUtil,
                                   PasswordEncoder passwordEncoder,
                                   SecurityContextRepository securityContextRepository) {
        this.repository = repository;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.passwordEncoder = passwordEncoder;
        this.securityContextRepository = securityContextRepository;
    }


    @Override
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        if (isAuthenticated(authentication)) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof UserDetails) {
                return ((SecuredUserDetails) principal).getUser();
            }
        }
        throw new SecurityDetailsNotFoundException("Can't find current user. Login again!");
    }

    public boolean isLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        return isAuthenticated(authentication);
    }

    @Override
    public AuthenticationDto login(String login, String password) throws BadCredentialsException {
        User user = repository.findByLogin(login)
                .orElseThrow(() -> new SecurityDetailsNotFoundException("Can't find user by login: " + login));
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(login, password);
        return authenticate(user, authenticationToken);
    }

    @Override
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private AuthenticationDto authenticate(User user, Authentication authToken) {
        try {
            Authentication authentication = authenticationManager.authenticate(authToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            Date expireTokenDate = new Date(System.currentTimeMillis() + jwtTokenUtil.getJwtTokenExpireTime() * 1000);

            String generatedToken = jwtTokenUtil.generateToken(user, expireTokenDate);
            return AuthenticationDto.builder()
                    .authToken(generatedToken)
                    .expireTime(expireTokenDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                    .build();
        } catch (BadCredentialsException ex) {
            throw new RuntimeException(ex);
        }
    }

    private boolean isAuthenticated(Authentication authentication) {
        return authentication != null
                && !(authentication instanceof AnonymousAuthenticationToken)
                && authentication.isAuthenticated();
    }

}
