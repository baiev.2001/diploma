package edu.chmnu.baiev.diploma.user.security;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class LoginDto {

    @NotEmpty
    private String login;
    @NotEmpty
    private String password;
}
