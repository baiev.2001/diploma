package edu.chmnu.baiev.diploma.user.security.service;

import edu.chmnu.baiev.diploma.core.entity.User;
import edu.chmnu.baiev.diploma.user.security.exception.InvalidTokenException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static java.util.Optional.ofNullable;

@Component
public class JwtUtil {
    @Value("${jwt.secret:secret}")
    private String secret;

    @Value("${jwt.expire-time:1800}")
    private long jwtTokenExpireTime;

    public long getIdFromToken(String token) {
        return Long.parseLong(getClaimFromToken(token, Claims::getSubject));
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    public String generateToken(User user, Date expireDate) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, String.valueOf(user.getId()), expireDate);
    }

    public <U extends User> boolean validateToken(String token, U user) {
        long id = getIdFromToken(token);
        return ofNullable(user)
                .map(u -> u.getId().equals(id))
                .orElse(false) && !isTokenExpired(token);
    }

    public long getJwtTokenExpireTime() {
        return jwtTokenExpireTime;
    }

    private Claims getAllClaimsFromToken(String token) {
        try {
            return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        } catch (Exception ex) {
            throw new InvalidTokenException(token, ex);
        }
    }


    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }


    private String doGenerateToken(Map<String, Object> claims, String subject, Date expireDate) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }


}
