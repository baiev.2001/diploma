package edu.chmnu.baiev.diploma.user.security.filter;

import edu.chmnu.baiev.diploma.core.entity.User;
import edu.chmnu.baiev.diploma.core.repository.UserRepository;
import edu.chmnu.baiev.diploma.user.security.exception.InvalidTokenException;
import edu.chmnu.baiev.diploma.user.security.SecuredUserDetails;
import edu.chmnu.baiev.diploma.user.security.service.JwtUtil;
import liquibase.repackaged.org.apache.commons.lang3.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

@Component
@Slf4j
public class UserAuthFilter extends OncePerRequestFilter {

    private AntPathRequestMatcher[] excludedPaths;
    private final JwtUtil jwtTokenUtil;

    private final UserRepository repository;

    public UserAuthFilter(JwtUtil jwtTokenUtil, UserRepository repository) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.repository = repository;
    }

    public void setExcludePathes(AntPathRequestMatcher... paths) {
        this.excludedPaths = paths;
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        if (excludedPaths != null) {
            return Arrays.stream(excludedPaths)
                    .anyMatch(matcher -> matcher.matches(request));
        }
        return false;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader("Authorization");
        if (StringUtils.isNotBlank(token)) {
            try {
                long userId = jwtTokenUtil.getIdFromToken(token);
                User user = repository.findById(userId)
                        .orElseThrow(() -> new RuntimeException("Can't find user! " + userId));
                if (Objects.isNull(user) || !jwtTokenUtil.validateToken(token, user)) {
                    throw new RuntimeException("Token is invalid. " + token);
                } else {
                    if (SecurityContextHolder.getContext().getAuthentication() == null) {
                        UserDetails userDetails = new SecuredUserDetails(user);
                        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(authToken);
                    }
                }
            } catch (InvalidTokenException exception) {
                log.error(exception.toString());
                filterChain.doFilter(request, response);
                return;
            }

        }

        filterChain.doFilter(request, response);
    }

}
