package edu.chmnu.baiev.diploma.user.data.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeviceDetails {
    private String chipId;

    private String type;

    private List<DataDTO> deviceData;
}
