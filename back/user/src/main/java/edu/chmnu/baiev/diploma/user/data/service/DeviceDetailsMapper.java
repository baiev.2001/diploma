package edu.chmnu.baiev.diploma.user.data.service;

import edu.chmnu.baiev.diploma.core.entity.Data;
import edu.chmnu.baiev.diploma.core.entity.Device;
import edu.chmnu.baiev.diploma.core.repository.DataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DeviceDetailsMapper {
    private final DataRepository dataRepository;
    private final DataMapper dataMapper;

    @Transactional(readOnly = true)
    public DeviceDetails map(Device device) {
        DeviceDetails details = new DeviceDetails();
        details.setChipId(device.getChipId());
        details.setType(device.getType().getName());
        List<Data> data = dataRepository.findByDevice(device.getId());
        details.setDeviceData(data.stream().map(dataMapper::map).collect(Collectors.toList()));
        return details;
    }
}
