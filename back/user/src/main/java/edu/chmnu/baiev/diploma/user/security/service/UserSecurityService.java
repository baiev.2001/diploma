package edu.chmnu.baiev.diploma.user.security.service;

import edu.chmnu.baiev.diploma.core.entity.User;
import edu.chmnu.baiev.diploma.user.security.AuthenticationDto;

public interface UserSecurityService {

    User getCurrentUser();

    boolean isLoggedIn();

    AuthenticationDto login(String login, String password);

    void logout();

}
