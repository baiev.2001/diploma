package edu.chmnu.baiev.diploma.user.security;

import edu.chmnu.baiev.diploma.user.security.filter.UserAuthFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class SecurityConf {
    public static final AntPathRequestMatcher[] PUBLIC_ROUTES = new AntPathRequestMatcher[]{
            new AntPathRequestMatcher("/h2-console/**"),
            new AntPathRequestMatcher("/auth/**"),
    };

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityContextRepository securityContextRepository() {
        return new HttpSessionSecurityContextRepository();
    }

    @Bean
    AuthenticationProvider authenticationProvider(PasswordEncoder encoder,
                                                  UserDetailsService service) {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setPasswordEncoder(encoder);
        authenticationProvider.setUserDetailsService(service);
        authenticationProvider.setAuthoritiesMapper(new SimpleAuthorityMapper());
        return authenticationProvider;
    }

    @Bean
    public SecurityFilterChain configureHttpSecurity(HttpSecurity http,
                                                     AuthenticationProvider authenticationProvider,
                                                     UserAuthFilter userAuthFilter) throws Exception {
        http.authenticationProvider(authenticationProvider);
        http.cors().and()
                .csrf().disable()
                .headers()
                .frameOptions().disable()
                .cacheControl();
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests()
                .antMatchers("/h2-console/**", "/auth/**")
                .permitAll();

        http.authorizeRequests()
                .antMatchers("/data/**", "/device/**")
                .hasRole("CLIENT");
        userAuthFilter.setExcludePathes(PUBLIC_ROUTES);
        http.addFilterBefore(userAuthFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }
}
