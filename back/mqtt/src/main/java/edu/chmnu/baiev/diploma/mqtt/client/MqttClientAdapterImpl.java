package edu.chmnu.baiev.diploma.mqtt.client;

import com.hivemq.client.mqtt.MqttClient;
import com.hivemq.client.mqtt.MqttGlobalPublishFilter;
import com.hivemq.client.mqtt.datatypes.MqttTopic;
import com.hivemq.client.mqtt.mqtt5.Mqtt5Client;
import com.hivemq.client.mqtt.mqtt5.message.publish.Mqtt5Publish;
import com.hivemq.client.mqtt.mqtt5.message.publish.Mqtt5PublishResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.function.Consumer;

import static java.nio.charset.Charset.defaultCharset;
import static java.nio.charset.StandardCharsets.UTF_8;

@Component
@Slf4j
public class MqttClientAdapterImpl implements MqttClientAdapter {
    private final String login;
    private final String password;

    private final Mqtt5Client client;
    private final MqttDataMapper dataMapper;

    public MqttClientAdapterImpl(Mqtt5Client client, MqttDataMapper dataMapper) {
        this.client = client;
        this.dataMapper = dataMapper;
//      FIXME: WTF!??!?!?!?
        this.login = defaultCharset().decode(client.getConfig()
                .getSimpleAuth().get()
                .getUsername().get()
                .toByteBuffer()).toString();
        this.password = defaultCharset().decode(client.getConfig()
                .getSimpleAuth().get()
                .getPassword().get()).toString();
    }

    @Autowired
    public MqttClientAdapterImpl(@Value("${mqtt.host}") String host,
                                 @Value("${mqtt.port:8883}") Integer port,
                                 @Value("${mqtt.login}") String login,
                                 @Value("${mqtt.password}") String password,
                                 MqttDataMapper dataMapper) {
        this.client = MqttClient.builder()
                .useMqttVersion5()
                .serverHost(host)
                .serverPort(port)
                .sslWithDefaultConfig()
                .build();
        this.login = login;
        this.password = password;
        this.dataMapper = dataMapper;
    }

    @PostConstruct
    public void setUp() {
        this.authorize(login, password);
    }

    @Override
    public void authorize(String login, String password) {
        client.toBlocking()
                .connectWith()
                .simpleAuth()
                .username(login)
                .password(UTF_8.encode(password))
                .applySimpleAuth()
                .send();
        log.info("Authorized MQTT client");
    }

    @Override
    public void subscribe(String topic) {
        client.toBlocking()
                .subscribeWith()
                .topicFilter(topic)
                .send();
        log.info("Subscribed for topic {}", topic);
    }

    @Override
    public void subscribe(MqttTopic topic) {
        client.toBlocking()
                .subscribeWith()
                .topicFilter(topic.filter())
                .send();
        log.info("Subscribed for topic {}", topic);
    }

    @Override
    public void publish(String topic, Object data) {
        Mqtt5PublishResult result = client.toBlocking()
                .publish(Mqtt5Publish.builder()
                        .topic(topic)
                        .payload(dataMapper.mapSending(data))
                        .build());
        log.info(result.toString());
    }

    @Override
    public void setCallback(Consumer<Mqtt5Publish> callback) {
        client.toAsync()
                .publishes(MqttGlobalPublishFilter.ALL, callback);
    }

    @Override
    @PreDestroy
    public void disconnect() {
        client.toBlocking().disconnect();
        log.info("Disconnected MQTT client");
    }
}
