package edu.chmnu.baiev.diploma.mqtt;

import com.hivemq.client.mqtt.MqttClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages = "edu.chmnu.baiev.diploma.mqtt")
@EntityScan("edu.chmnu.baiev.diploma.core")
@EnableJpaRepositories
public class Conf {


}
