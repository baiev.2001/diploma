package edu.chmnu.baiev.diploma.mqtt.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.ByteBuffer;

@Component
@RequiredArgsConstructor
public class JsonDataMapper implements MqttDataMapper {
    private final ObjectMapper mapper;

    @Override
    public <T> T mapReceived(Object data, Class<T> targetClass) {
        try {
            if (data instanceof byte[]) {
                return mapper.readValue((byte[]) data, targetClass);
            }
            if (data instanceof ByteBuffer) {
                ByteBuffer buffer = (ByteBuffer) data;
                return mapper.readValue(buffer.array(), targetClass);
            }
            return mapper.readValue(data.toString(), targetClass);
        } catch (IOException exception) {
            throw new RuntimeException("Error during params mapping", exception);
        }
    }

    @Override
    public byte[] mapSending(Object data) {
        try {
            return mapper.writeValueAsBytes(data);
        } catch (JsonProcessingException exception) {
            throw new RuntimeException("Error during mapping payload!", exception);
        }

    }
}
