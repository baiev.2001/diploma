package edu.chmnu.baiev.diploma.mqtt.service;

public interface DataService {

    void updateData(DataDTO dto);
}
