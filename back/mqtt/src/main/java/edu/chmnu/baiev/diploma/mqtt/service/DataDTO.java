package edu.chmnu.baiev.diploma.mqtt.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class DataDTO {
    private Integer type;

    private String chipId;

    private Double value;
}
