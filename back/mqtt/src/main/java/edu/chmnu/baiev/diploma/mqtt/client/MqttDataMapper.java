package edu.chmnu.baiev.diploma.mqtt.client;

public interface MqttDataMapper {

    <T> T mapReceived(Object data, Class<T> targetClass);

    byte[] mapSending(Object data);
}
