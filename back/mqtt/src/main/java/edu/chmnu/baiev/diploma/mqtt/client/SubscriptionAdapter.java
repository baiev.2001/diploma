package edu.chmnu.baiev.diploma.mqtt.client;

import edu.chmnu.baiev.diploma.mqtt.client.MqttDataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@RequiredArgsConstructor
public class SubscriptionAdapter {
    private final MqttDataMapper inputDataMapper;
    private final Object target;
    private final Method method;

    public Object execute(Object inputData) {
        if (method.getParameterCount() > 1) {
            throw new RuntimeException("Can't invoke method. Too much params");
        }
        Parameter param = method.getParameters()[0];
        return ReflectionUtils.invokeMethod(method, target, inputDataMapper.mapReceived(inputData, param.getType()));
    }
}
