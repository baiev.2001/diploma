package edu.chmnu.baiev.diploma.mqtt.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hivemq.client.mqtt.datatypes.MqttTopic;
import edu.chmnu.baiev.diploma.mqtt.MqttSubscribe;
import org.springframework.aop.support.AopUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodIntrospector;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Configuration
public class ClientConfiguration {

    @Bean(name = "topicMethods")
    public Map<MqttTopic, SubscriptionAdapter> setupSubscriptions(ApplicationContext context,
                                                                  MqttClientAdapter client,
                                                                  MqttDataMapper dataMapper) {
        Map<MqttTopic, SubscriptionAdapter> topicMethodMap = new HashMap<>();
        Map<String, Object> beansOfType = context.getBeansOfType(Object.class);
        for (Object bean : beansOfType.values()) {
            if (AopUtils.isAopProxy(bean)) {
                continue;
            }
            Set<Method> annotatedMethods = MethodIntrospector.selectMethods(bean.getClass(),
                    (Method method) -> method.getDeclaredAnnotation(MqttSubscribe.class) != null);
            if (annotatedMethods.isEmpty()) {
                continue;
            }
            for (Method method : annotatedMethods) {
                MqttSubscribe subscribtionProperties = method.getAnnotation(MqttSubscribe.class);
                MqttTopic topic = MqttTopic.of(subscribtionProperties.topic());
                if (topicMethodMap.containsKey(topic)) {
                    throw new RuntimeException("It is allowed only one method per topic in one application");
                }
                client.subscribe(topic);
                topicMethodMap.put(topic, new SubscriptionAdapter(dataMapper, bean, method));
            }
        }
        //      TODO: add bean for callbacks
        client.setCallback(message -> {
            if (!topicMethodMap.containsKey(message.getTopic())) {
                throw new RuntimeException("Can't find corresponding method for topic");
            }
            topicMethodMap.get(message.getTopic()).execute(message.getPayloadAsBytes());
        });

        return topicMethodMap;
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        return mapper;
    }
}
