package edu.chmnu.baiev.diploma.mqtt.client;

import com.hivemq.client.mqtt.datatypes.MqttTopic;
import com.hivemq.client.mqtt.mqtt5.message.publish.Mqtt5Publish;

import java.util.function.Consumer;

public interface MqttClientAdapter {

    void authorize(String login, String password);

    void subscribe(String topic);

    void subscribe(MqttTopic topic);

    void publish(String topic, Object data);

    void setCallback(Consumer<Mqtt5Publish> callback);

    void disconnect();
}
