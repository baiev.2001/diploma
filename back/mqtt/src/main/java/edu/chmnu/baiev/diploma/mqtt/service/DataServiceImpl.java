package edu.chmnu.baiev.diploma.mqtt.service;

import edu.chmnu.baiev.diploma.core.entity.Data;
import edu.chmnu.baiev.diploma.core.entity.Device;
import edu.chmnu.baiev.diploma.core.entity.DeviceType;
import edu.chmnu.baiev.diploma.core.repository.DataRepository;
import edu.chmnu.baiev.diploma.core.repository.DeviceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class DataServiceImpl implements DataService {
    private final DeviceRepository deviceRepository;
    private final DataRepository dataRepository;

    @Override
    @Transactional
    public void updateData(DataDTO dto) {
        Device device = deviceRepository.findByChipId(dto.getChipId());
        DeviceType type = DeviceType.of(dto.getType());
        if (!type.equals(device.getType())) {
            device.setType(type);
        }
        Data data = new Data();
        data.setDevice(device);
        data.setVal(data.getVal());
        dataRepository.save(data);
    }
}
