package edu.chmnu.baiev.diploma.mqtt.controller;

import edu.chmnu.baiev.diploma.mqtt.client.MqttClientAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class Test {
    private final MqttClientAdapter clientAdapter;


    @GetMapping("/test")
    public String helloWorld() {
        clientAdapter.publish("data/update", "Hello");
        return "Hellowrodl";
    }
}
