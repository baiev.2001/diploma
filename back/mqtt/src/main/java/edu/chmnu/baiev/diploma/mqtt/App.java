package edu.chmnu.baiev.diploma.mqtt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication

public class App {

    public static void main(String[] args) {
//        final String host = "84bc8b24816f4736b3605deec40ca17e.s1.eu.hivemq.cloud";
//        final String username = "baiev.v";
//        final String password = "Bav_vit231";
//
//        //create an MQTT client
//        final Mqtt5BlockingClient client = MqttClient.builder()
//                .useMqttVersion5()
//                .serverHost(host)
//                .serverPort(8883)
//                .sslWithDefaultConfig()
//                .buildBlocking();
//
//        //connect to HiveMQ Cloud with TLS and username/pw
//        client.connectWith()
//                .simpleAuth()
//                .username(username)
//                .password(UTF_8.encode(password))
//                .applySimpleAuth()
//                .send();
//
//        System.out.println("Connected successfully");
//
//        //subscribe to the topic "my/test/topic"
//        client.subscribeWith()
//                .topicFilter("my/test/topic")
//                .send();
//
//        //set a callback that is called when a message is received (using the async API style)
//        client.toAsync().publishes(MqttGlobalPublishFilter.ALL, publish -> {
//            System.out.println("Received message: " + publish.getTopic() + " -> " + UTF_8.decode(publish.getPayload().get()));
//
//            //disconnect the client after a message was received
//            client.disconnect();
//        });
//
//        //publish a message to the topic "my/test/topic"
//        client.publishWith()
//                .topic("my/baiev/topic")
//                .payload(UTF_8.encode("Hello"))
//                .send();
        SpringApplication.run(App.class);

    }
}

