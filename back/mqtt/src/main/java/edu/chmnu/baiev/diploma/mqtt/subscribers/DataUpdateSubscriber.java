package edu.chmnu.baiev.diploma.mqtt.subscribers;

import edu.chmnu.baiev.diploma.mqtt.MqttSubscribe;
import edu.chmnu.baiev.diploma.mqtt.service.DataDTO;
import edu.chmnu.baiev.diploma.mqtt.service.DataService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DataUpdateSubscriber {
    private final DataService dataService;

    @MqttSubscribe(topic = "data/test")
    public void testMethod(String obj) {
        System.out.println("Received message: " + obj);
    }

    @MqttSubscribe(topic = "data/update")
    public void updateData(DataDTO data) {
        dataService.updateData(data);
    }
}
